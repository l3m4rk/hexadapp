package me.l3m4rk.test.hexadapp

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import me.l3m4rk.test.hexadapp.di.app.DaggerAppComponent
import timber.log.Timber

class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<App> {
        return DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
    }
}