package me.l3m4rk.test.hexadapp.data

import me.l3m4rk.test.hexadapp.models.favorites.FavoriteModel
import me.l3m4rk.test.hexadapp.models.favorites.UpdateRatingEvent
import timber.log.Timber
import java.util.*

object HeapStorage {

    private val ITEMS = Collections.synchronizedList(
        arrayListOf(
            FavoriteModel(id = 1, title = "The Lord of the Rings, J.R.R. Tolkien"),
            FavoriteModel(id = 2, title = "Nine Princes in Amber, Roger Zelazny"),
            FavoriteModel(id = 3, title = "Dune, Frank Herbert"),
            FavoriteModel(id = 4, title = "The Avengers"),
            FavoriteModel(id = 5, title = "Thor Ragnarok"),
            FavoriteModel(id = 6, title = "Nineteen Eighty-Four, George Orwell"),
            FavoriteModel(id = 7, title = "Iron man"),
            FavoriteModel(id = 8, title = "The Bull's Hour, Ivan Yefremov"),
            FavoriteModel(id = 9, title = "Clean Code, Robert Martin"),
            FavoriteModel(id = 10, title = "The Godfather")
        )
    )

    fun items(): List<FavoriteModel> = ITEMS

    fun updateRating(event: UpdateRatingEvent) {
        ITEMS.find { it.id == event.id }
            ?.also { it.rating = event.rating } ?: Timber.i("Not updated!")
    }


}