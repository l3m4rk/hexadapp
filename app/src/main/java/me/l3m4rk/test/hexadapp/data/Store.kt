package me.l3m4rk.test.hexadapp.data

import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

interface Store<T> {

    fun getAll(): Maybe<T>

    fun putAll(list: List<T>)

    fun clear()

}

interface ReactiveStore<V> {

    fun storeAll(items: List<V>)

    fun getAll(): Observable<List<V>>

}

class InMemoryReactiveStore<V> : ReactiveStore<V> {

    private val allSubject = PublishSubject.create<List<V>>().toSerialized()

    override fun storeAll(items: List<V>) {

    }

    override fun getAll(): Observable<List<V>> {
        TODO()
    }
}