package me.l3m4rk.test.hexadapp.di.app

import dagger.Module
import dagger.android.ContributesAndroidInjector
import me.l3m4rk.test.hexadapp.di.favorites.FavoritesModule
import me.l3m4rk.test.hexadapp.presentation.MainActivity
import me.l3m4rk.test.hexadapp.presentation.favorites.FavoritesFragment

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [FavoritesModule::class])
    abstract fun bindFavorites(): FavoritesFragment

}