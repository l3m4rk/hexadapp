package me.l3m4rk.test.hexadapp.di.favorites

import dagger.Binds
import dagger.Module
import dagger.Provides
import me.l3m4rk.test.hexadapp.domain.FavoritesInteractor
import me.l3m4rk.test.hexadapp.domain.FavoritesInteractorImpl
import me.l3m4rk.test.hexadapp.presentation.favorites.FavoritesFragment
import me.l3m4rk.test.hexadapp.presentation.favorites.FavoritesViewModel
import me.l3m4rk.test.hexadapp.repositories.FavoritesRepository
import me.l3m4rk.test.hexadapp.repositories.FavoritesRepositoryImpl
import me.l3m4rk.test.hexadapp.presentation.common.viewModel

@Module
abstract class FavoritesModule {

    @Binds
    abstract fun bindRepository(repositoryImpl: FavoritesRepositoryImpl): FavoritesRepository

    @Binds
    abstract fun bindInteractor(interactorImpl: FavoritesInteractorImpl): FavoritesInteractor

    @Module
    companion object {

        @Provides
        fun providesViewModel(fragment: FavoritesFragment, interactor: FavoritesInteractor) =
            fragment.viewModel { FavoritesViewModel(interactor) }
    }

}