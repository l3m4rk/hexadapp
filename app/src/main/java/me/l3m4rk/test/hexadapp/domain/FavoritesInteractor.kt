package me.l3m4rk.test.hexadapp.domain

import io.reactivex.Completable
import io.reactivex.Observable
import me.l3m4rk.test.hexadapp.models.favorites.FavoriteItem
import me.l3m4rk.test.hexadapp.models.favorites.UpdateRatingEvent
import me.l3m4rk.test.hexadapp.repositories.FavoritesRepository
import javax.inject.Inject

interface FavoritesInteractor {

    val favoritesList: Observable<List<FavoriteItem>>

    fun updateRatingOnFavorite(event: UpdateRatingEvent): Completable

}

class FavoritesInteractorImpl @Inject constructor(
    private val repository: FavoritesRepository
) : FavoritesInteractor {

    private val descRatingSortStrategy = { items: List<FavoriteItem> ->
        items.sortedByDescending { it.rating }
    }

    override val favoritesList: Observable<List<FavoriteItem>>
        get() = repository.items.map { rateModels ->
            rateModels.map {
                FavoriteItem(it.id, it.title, it.rating)
            }
        }.map(descRatingSortStrategy)

    override fun updateRatingOnFavorite(event: UpdateRatingEvent): Completable {
        return Completable.fromAction { repository.updateRatingOfFavorite(event) }
    }

}
