package me.l3m4rk.test.hexadapp.models.favorites

data class FavoriteItem(
    val id: Long,
    val title: String,
    val rating: Float
)