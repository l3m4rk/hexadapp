package me.l3m4rk.test.hexadapp.models.favorites

data class FavoriteModel(
    val id: Long,
    val title: String,
    var rating: Float = 0.0F
)