package me.l3m4rk.test.hexadapp.models.favorites

data class UpdateRatingEvent(val id: Long, val rating: Float)