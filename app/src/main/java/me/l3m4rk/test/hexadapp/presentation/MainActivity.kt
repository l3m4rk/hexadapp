package me.l3m4rk.test.hexadapp.presentation

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import me.l3m4rk.test.hexadapp.R
import me.l3m4rk.test.hexadapp.presentation.favorites.FavoritesFragment

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (null == savedInstanceState) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.contatiner, FavoritesFragment())
                .commit()
        }
    }
}
