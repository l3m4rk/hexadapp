package me.l3m4rk.test.hexadapp.presentation.favorites

import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_favorite.view.*
import me.l3m4rk.test.hexadapp.R
import me.l3m4rk.test.hexadapp.models.favorites.FavoriteItem
import me.l3m4rk.test.hexadapp.models.favorites.UpdateRatingEvent
import timber.log.Timber
import javax.inject.Inject

private const val RATING = "RATING"

class FavoritesAdapter @Inject constructor() : RecyclerView.Adapter<FavoritesAdapter.FavoriteViewHolder>() {

    private var items: List<FavoriteItem>? = null
    private val ratingSubject = PublishSubject.create<UpdateRatingEvent>()

    val ratingEvents: Observable<UpdateRatingEvent> = ratingSubject

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_favorite, parent, false)
            .let { FavoriteViewHolder(it, ratingSubject) }

    override fun getItemCount(): Int = items!!.size

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        holder.bind(items!![position])
    }

    fun update(newItems: List<FavoriteItem>) {
        if (items == null) {
            items = newItems
            notifyDataSetChanged()
        } else {
            val result = DiffUtil.calculateDiff(FavoritesCallback(items!!, newItems))
            items = newItems
            result.dispatchUpdatesTo(this)
            logRatings()
        }
    }

    private fun logRatings() {
        Timber.w("Ratings")
        items!!.forEach {
            Timber.w(it.rating.toString().plus("-${it.title}"))
        }
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            Timber.i("Payloads is empty! position $position")
            onBindViewHolder(holder, position)
        } else {
            Timber.i("Payloads is not empty!")
            (payloads.first() as Bundle?)?.also {
                holder.updateRating(it.getFloat(RATING))
            } ?: Timber.w("There is no new rating!")
        }
    }

    class FavoriteViewHolder(
        private val view: View,
        private val ratingSubject: PublishSubject<UpdateRatingEvent>
    ) : RecyclerView.ViewHolder(view) {

        private var id: Long? = null

        init {
            itemView.setOnClickListener {
                Timber.i("Item clicker")
            }

            itemView.ratingBar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
                if (fromUser) {
                    val ratingEvent = id?.let { id -> UpdateRatingEvent(id, rating) }!!
                    Timber.i("Rating event $ratingEvent")
                    ratingSubject.onNext(ratingEvent)
                }
            }
        }

        fun bind(item: FavoriteItem) {
            id = item.id
            itemView.title.text = item.title
            itemView.ratingBar.rating = item.rating
        }

        fun updateRating(rating: Float) {
            view.ratingBar.rating = rating
        }

    }

}

class FavoritesCallback(
    private val oldFavorites: List<FavoriteItem>,
    private val newFavorites: List<FavoriteItem>
) : DiffUtil.Callback() {

    companion object {
        const val RATING = "RATING"
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldFavorites[oldItemPosition].id == newFavorites[newItemPosition].id
    }

    override fun getOldListSize(): Int = oldFavorites.size
    override fun getNewListSize(): Int = newFavorites.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldFavorites[oldItemPosition].rating == newFavorites[newItemPosition].rating
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        Timber.i("From $oldItemPosition to $newItemPosition – getChangePayload called")
        val oldItem = oldFavorites[oldItemPosition]
        val newItem = newFavorites[newItemPosition]

        val diffBundle = Bundle()
        if (oldItem.rating != newItem.rating) {
            diffBundle.apply { putFloat(RATING, newItem.rating) }
        }

        return if (diffBundle.isEmpty) null else diffBundle
    }
}