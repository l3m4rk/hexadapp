package me.l3m4rk.test.hexadapp.presentation.favorites

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.favorites_fragment.*
import me.l3m4rk.test.hexadapp.R
import javax.inject.Inject

class FavoritesFragment : DaggerFragment() {

    @Inject
    lateinit var adapter: FavoritesAdapter

    @Inject
    lateinit var viewModel: FavoritesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.favorites_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupTitle()
        hideProgress()
        setupList()
        setupRandomButton()
        observeItems()
    }

    private fun setupTitle() {
        activity?.title = getString(R.string.favorites_title)
    }

    private fun hideProgress() {
        progress.visibility = View.GONE
    }

    private fun setupList() {
        favoritesList.itemAnimator = DefaultItemAnimator()
        favoritesList.layoutManager = LinearLayoutManager(context)
        favoritesList.adapter = adapter
        viewModel.onUpdateRatingClicked(adapter.ratingEvents)
    }

    private fun setupRandomButton() {
        randomButton.setOnClickListener {
            viewModel.onRandomRatingClicked()
        }
        viewModel.rateInProgress.observe(this, Observer {
            it?.let { inProgress ->
                randomButton.text = if (inProgress) {
                    getString(R.string.rating_button_progress_text)
                } else {
                    getString(R.string.rating_button_initial_text)
                }
            }
        })
    }

    private fun observeItems() {
        viewModel.viewState.observe(this, Observer {
            it?.let { viewState ->
                renderViewState(viewState)
            }
        })
    }

    private fun renderViewState(viewState: ViewState) {
        when (viewState) {
            is ViewState.Loading -> {
                showProgress()
            }
            is ViewState.Success -> {
                hideProgress()
                adapter.update(viewState.items)
            }
            is ViewState.Error -> {
                hideProgress()
                Toast.makeText(context, viewState.message, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
    }

}