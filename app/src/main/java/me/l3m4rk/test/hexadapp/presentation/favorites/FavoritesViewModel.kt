package me.l3m4rk.test.hexadapp.presentation.favorites

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import me.l3m4rk.test.hexadapp.domain.FavoritesInteractor
import me.l3m4rk.test.hexadapp.models.favorites.UpdateRatingEvent
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.random.Random

class FavoritesViewModel @Inject constructor(
    private val interactor: FavoritesInteractor
) : ViewModel() {

    private val disposables = CompositeDisposable()

    private var randomRatingInProgress = false

    private lateinit var randomRatingDisposable: Disposable

    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = _viewState

    private val _rateInProgress = MutableLiveData<Boolean>()
    val rateInProgress: LiveData<Boolean> = _rateInProgress

    init {
        disposables += bindFavorites()
    }

    private fun bindFavorites(): Disposable {
        return interactor.favoritesList
            .map { ViewState.Success(it) as ViewState }
            .onErrorReturn {
                val message = it.localizedMessage
                ViewState.Error(message)
            }
            .startWith(ViewState.Loading)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                _viewState.value = it
            }
    }

    fun onUpdateRatingClicked(updateRatingEvents: Observable<UpdateRatingEvent>) {
        disposables += updateRatingEvents
            .flatMapCompletable { interactor.updateRatingOnFavorite(it) }
            .subscribe()
    }

    fun onRandomRatingClicked() {
        startUpdateRatingRandomly()
    }

    private fun startUpdateRatingRandomly() {
        if (randomRatingInProgress) {
            randomRatingInProgress = !randomRatingInProgress
            _rateInProgress.value = randomRatingInProgress

            disposables.remove(randomRatingDisposable)
        } else {
            randomRatingInProgress = !randomRatingInProgress
            _rateInProgress.value = randomRatingInProgress

            randomRatingDisposable = Observable.interval(1, TimeUnit.SECONDS)
                .delay(Random.nextLong(1, 5), TimeUnit.SECONDS)
                .map {
                    createRandomUpdateRatingEvent()
                }
                .subscribeOn(Schedulers.single())
                .flatMapCompletable { interactor.updateRatingOnFavorite(it) }
                .subscribe()

            disposables += randomRatingDisposable
        }
    }

    private fun createRandomUpdateRatingEvent(): UpdateRatingEvent {
        val randomId = Random.nextLong(1, 5)
        val randomRating = Random.nextInt(1, 5).toFloat()

        val randomEvent = UpdateRatingEvent(
            id = randomId,
            rating = randomRating
        )

        Timber.i("Random event $randomEvent")
        return randomEvent
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

}