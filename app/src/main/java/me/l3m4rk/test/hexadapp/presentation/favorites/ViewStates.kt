package me.l3m4rk.test.hexadapp.presentation.favorites

import me.l3m4rk.test.hexadapp.models.favorites.FavoriteItem

sealed class ViewState {

    data class Success(val items: List<FavoriteItem>) : ViewState()

    data class Error(val message: String) : ViewState()

    object Loading : ViewState()

}