package me.l3m4rk.test.hexadapp.repositories

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import me.l3m4rk.test.hexadapp.data.HeapStorage
import me.l3m4rk.test.hexadapp.models.favorites.FavoriteModel
import me.l3m4rk.test.hexadapp.models.favorites.UpdateRatingEvent
import javax.inject.Inject

interface FavoritesRepository {

    val items: Observable<List<FavoriteModel>>

    fun updateRatingOfFavorite(event: UpdateRatingEvent)

}

class FavoritesRepositoryImpl @Inject constructor() : FavoritesRepository {

    private val subject = PublishSubject.create<List<FavoriteModel>>().toSerialized()

    override val items: Observable<List<FavoriteModel>> = subject.startWith(HeapStorage.items())

    override fun updateRatingOfFavorite(event: UpdateRatingEvent) {
        HeapStorage.updateRating(event)
        subject.onNext(HeapStorage.items())
    }
}
